<?php


return [


	/*
	 * Default plivo provider to select from
	 * Only available option right now is 'plivo'
	 */
	'default'	=> 'plivo',

	'plivo'		=> [
		'auth_id'	=> env('SMS_AUTH_ID', ''),
		'auth_token'=> env('SMS_AUTH_TOKEN', ''),
		'src'		=> env('SMS_SRC', ''),
		'method'	=> env('SMS_METHOD', 'POST')
	]

];