<?php namespace Hvh;

use Plivo\RestAPI;
use Config;

class Sms
{

	public function __construct($to)
	{
		// new RestAPI()
		$default = Config::get('sms.default');

		$auth_id = Config::get("sms.$default.auth_id");
		$auth_token = Config::get("sms.$default.auth_token");
		$src = Config::get("sms.$default.src");
		$method = Config::get("sms.$default.method");

		$this->api = new RestAPI($auth_id, $auth_token);

		$this->params = [
			'src' => $src,
			'dst' => $to,
			'method' => $method
		];
	}

	public function send_message($message)
	{
		$this->params['text'] = (string)$message;
		$this->api->send_message($this->params);
	}

	public static function send($to, $message)
	{
		$instance = new static($to);
		return $instance->send_message($message);
	}

	public static function sendOtp($to)
	{
		$instance = new static($to);
		$otp = rand(10000, 99999);
		$message = "Your OTP number is $otp";
		$instance->send_message($message);

		return $otp;
	}
}