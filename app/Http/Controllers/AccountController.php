<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Requests\AccountCreationRequest;
use App\Http\Controllers\Controller;

use App\Account;
use App\Number;

use Hvh\Sms;

class AccountController extends Controller
{
    public function postCreate(AccountCreationRequest $request)
    {
    	// return \Input::all();
	    $account = Account::create([
	    	'full_name' => $request->get('full_name'),
	    	'email' => $request->get('email'),
	    	'dob' => $request->get('dob'),
	    	'gender' => $request->get('gender'),
	    	'pan_no' => $request->get('pan_no')
	    ]);

	    $account->phone = new Number;
	    $otp = Sms::sendOtp("919880565726");

	    $account->phone->mobile = $request->get('mobile');
	    $account->phone->save();


	    return [
	    	'success' => true,
	    	'data' => $account
	    ];
    }

	public function postUpload()
	{
		return "Uploading Files...";

		$path_uri  = "Image_" . time() . '.jpg';

		$file_base64 = Input::get('file');

		list($type, $file_base64) = explode(';', $file_base64);
		list(, $file_base64)      = explode(',', $file_base64);
		$file = base64_decode($file_base64);

		file_put_contents('/tmp/' . $path_uri , $data);


		// Store in 'Image' model
		$photo = File::create(['filename' => $path_uri]);

		// Try relating if possible
		// if( Input::get('type') === "Product" ) {
		// 	$product = Product::find( Input::get("id") );

		// 	if($product){
		// 		$product->images()->save($photo);
		// 	}
		// }

		// Image class
		// Image::make($file))->save( public_path() . '/uploads/' . $path_uri);

		return ["success" => true, "id" => $photo->id];
	}

}
