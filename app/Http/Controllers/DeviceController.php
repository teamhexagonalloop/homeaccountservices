<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\GetAccessToken;

use Hvh\Token;
use Hvh\Sms;

use App\Device;
use App\Otp;

use Input;

class DeviceController extends Controller
{
    /**
     * Generates a Access token for given device id
     *
     * @return \Illuminate\Http\Response
     */
    public function postGetAccessToken()
    {
        $device_id = Input::get('device_id');

        // Create a find a device!
        $device = Device::where('did',$device_id)->first();

        if( !$device ) {
            $device = new Device(['did' => $device_id]);
        }

        $token = Token::generate();

        $device->token = $token;
        $device->token_timestamp = date('Y-m-d H:i:s');

        $success = $device->save();

        return [
            "success" => $success,
            "data" => [
                'did' => $device->did,
                'token' => $token,
                'expires' => 'After 5 minutes'
            ]
        ];
    }

    public function postVerifyNumber()
    {
        $number = Input::get('number');

        if(strlen($number) === 10) {
            $number = "91" . $number;
        }

        $device_id = Input::get('device_id');

        $device = Device::where('did', $device_id)->first();

        $otp = Sms::sendOtp("919880565726");

        $created_otp = Otp::create([
            'device_id' => $device->id,
            'otp'       => $otp,
            'otp_timestamp' => date('Y-m-d H:i:s'),
        ]);

        return $created_otp;
    }

    public function postVerifyOtp()
    {
        $otp_number = Input::get('otp_number');

        $otp = Otp::where('otp', $otp_number)->first();

        $half_minutes_ago = strtotime('-30 seconds');

        if($otp) $stillValid = $otp->otp_time->timestamp > $half_minutes_ago;

        return [
            'success' => $otp && $stillValid,
            'data' => $otp
        ];
    }

    function postTest()
    {
        return "Testing";
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
