<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as BaseVerifier;

class VerifyCsrfToken extends BaseVerifier
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        'api/v1/device/get-access-token'
    ];


    /**
     * Determine if the session and input CSRF tokens match.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return bool
     */
    protected function tokensMatch($request)
    {
        $token = $request->input('_token') ?: $request->header('X-CSRF-TOKEN');
        $device_id = $request->input('did');

        $device = \App\Device::where('did',$device_id)->first();

        // return $device && $device->token === $token && $this->isStillValid($device);
        return $device;
    }

    protected function isStillValid($device)
    {
    	$five_minutes_ago = strtotime('-5 minutes');

    	$stillValid = $device->token_time->timestamp > $five_minutes_ago;

    	// echo $device->token_time->timestamp; 

    	if(!$stillValid) {
    		throw new \Exception("Token has been expired!");
    	}

    	return $stillValid;
    }
}
