<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function(){
	return Hvh\Test::hello();
});

Route::get('/pdf', function(){
	return view('pdf');
});

Route::get('sms_test', function(){
	return Hvh\Sms::send(919880565726, "hello vinay");
});


Route::group(['prefix' => '/api'], function(){

	Route::group(['prefix' => '/v1'], function(){

		Route::get('/', function(){
			return [
				"success" => true,
				"message" => "Customer authentication api 'GET /api/v1'"
			];
		});

		Route::controller('device', 'DeviceController');
		Route::controller('account', 'AccountController');

	});

});