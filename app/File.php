<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class File extends Model {

	public $fillable = ['filename'];

	public function imageable()
	{
		return $this->morphTo();
	}

	function getUrlAttribute()
	{
		return asset('/uploads/' . $this->filename);
	}

}
