<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Account extends Model
{
    public $fillable = [
    	'full_name',
    	'email',
    	'dob',
    	'gender',
    	'pan_no',
    ];

    public function phone()
    {
    	return $this->hasOne('App\Number');
    }

    public function images()
    {
        return $this->morphMany('App\File', 'imageable');
    }

}
