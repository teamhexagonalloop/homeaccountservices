<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Otp extends Model
{
	
	public $fillable = [
		'device_id',
		'otp',
		'otp_timestamp'
	];

    public function getOtpTimeAttribute()
    {
		return new \Carbon\Carbon($this->otp_timestamp);
    }

}
