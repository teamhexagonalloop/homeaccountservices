<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Device extends Model
{
    public $fillable = [
    	'did',
    	'token',
    	'token_timestamp'
    ];

    public function getTokenTimeAttribute()
    {
		return new \Carbon\Carbon($this->token_timestamp);
    }
}
